from django.test import TestCase

from management.models import books
# Create your tests here.

class BookClassTest(TestCase):
    book_isbn = 12345
    book_title = "ABC_Book"
    book_author = "xyz"
    book_copys = 0
    modified_title = "ABC Book by XYZ"
    def test_add_books(self):
        k = books.objects.create(book_isbn=self.book_isbn,
                                 book_title=self.book_title,
                                 book_copys=self.book_copys,
                                 book_author=self.book_author)
        k.save()
        assert k in books.objects.all()

    def test_modify_book(self):
        k = books.objects.create(book_isbn=self.book_isbn,
                                 book_title=self.book_title,
                                 book_copys=self.book_copys,
                                 book_author=self.book_author)
        k.save()
        k = books.objects.get(book_isbn = self.book_isbn)
        k.book_title = self.modified_title
        k.save()

        j = books.objects.get(book_isbn = self.book_isbn)
        assert self.modified_title == j.book_title

    def test_book_delete(self):
        k = books.objects.create(book_isbn=self.book_isbn,
                                 book_title=self.book_title,
                                 book_copys=self.book_copys,
                                 book_author=self.book_author)
        k.save()
        k = books.objects.get(book_isbn = self.book_isbn)
        k.delete()

        k = books.objects.filter(book_isbn = self.book_isbn)
        assert len(k) == 0
